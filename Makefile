CC =g++ -g 

CFLAGS = -O3 -Wall -D_NOTHREADS -lc -fPIC -std=c++0x

OBJ = src/CFileReader.o src/CTrist.o src/CFmtConverter.o src/VectorOp.o src/reebgraph.o 

PRG = libReebGraph.so

all:

	$(CC) $(CFLAGS) -c src/CFileReader.cpp -o src/CFileReader.o
	$(CC) $(CFLAGS) -c src/CTrist.cpp -o src/CTrist.o
	$(CC) $(CFLAGS) -c src/CFmtConverter.cpp -o src/CFmtConverter.o
	$(CC) $(CFLAGS) -c src/VectorOp.cpp -o src/VectorOp.o
	$(CC) $(CFLAGS) -c src/reebgraph.cpp -o src/reebgraph.o
	$(CC) $(OBJ) -shared -o $(PRG)

clean:
		rm -f $(OBJ) $(PRG)
