/*
 *	Copyright (C) 2010 Visualization & Graphics Lab (VGL), Indian Institute of Science
 *
 *	This file is part of libRG, a library to compute Reeb graphs.
 *
 *	libRG is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Lesser General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	libRG is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Lesser General Public License for more details.
 *
 *	You should have received a copy of the GNU Lesser General Public License
 *	along with libRG.  If not, see <http://www.gnu.org/licenses/>.
 *
 *	Author(s):	Aneesh Sood
 *	Version	 :	1.0
 *
 *	Modified by : Aneesh Sood.
 *	Date : 14th May 2011.
 *	Changes  : Added code to clear the contents of all containers at the beginning of 
 *  		   the execution. 
 */
 
#include <stdio.h>
#include <algorithm>
#include <fstream>
#include <iostream>
#include "CFmtConverter.hpp"
#include "CFileReader.hpp"
#include "VectorOp.hpp"
#include "CTrist.hpp"
#include <map>
#include <sys/time.h>

using namespace std;

std::multimap<int, int> Tri_Info;
//std::multimap<int, int> Edge_Adjacency;
//std::map<int, int> EdgeID[60000];
int IdentityNo = 1;
int Type = 0;

const char* name;
vector<Vertex> m_vertlist;
vector<int> m_trilist;

/*
 * For a given edge, what is the corresponding position of the edge
 * in tri-star. e.g. The edge 0-1 is at position 0, edge 1-2 is at
 * position 2, edge 1-0 is at position 1 etc
 */
unsigned int CFmtConverter::edgepos[3][3] = {{-1,0,5}, {1,-1,2}, {4,3,-1}};

/*
 * For a given position pos, posedge[pos][0] and posedge[pos][1] gives the
 * edges curresponding to the position. This is the inverse of edgepos
 * which gives the position given an edge. So position 6 is edge 0-2
 */
unsigned int CFmtConverter::posedge[6][2] = {{0,1}, {1,0}, {1,2}, {2,1}, {2,0}, {0,2}};

/*
 * for the edge we are considering in a triangle, which is the adjacent 
 * triangle in the tet that would be the fnext?
 * for e.g. if we are considering edge 0-1 of triangle 0 (nodes 0,1,2), 
 * the fnext triangle is the one with nodes 0,1,3. This is obtained by
 * indexing into fnextpos with the vertex not belonging to the edge
 * we are considering (for edge 0-1, the vertex we are not considering
 * is 2. Hence fnextpos[0][2] == 1 == triangle with nodes 0,1,3
 * (numbering of the triangle is based on nodes given by nodepos. Triangle 1
 * has nodes 0,1,3 since nodepos[1] == 0,3,1)
 */ 
unsigned int CFmtConverter::fnextpos[4][3] = {{3,2,1}, {3,0,2}, {3,1,0}, {1,2,0}};

/*
 * The position of the nodes in the tet, for the triangle we are considering
 * The tet has 4 triangles, indexed by 0, 1, 2 and 3. The nodes of each triangle
 * are given by nodepos. The 4 triangles are obtained by splitting the tet and
 * the ordering of the nodes is in such a way that the orientation
 * is maintained. i.e. w.r.t the vertex not belonging to the triangle
 * the other 3 vertices will form an anti-clockwise turn
 */
unsigned int CFmtConverter::nodepos[4][3] = {{0,1,2}, {0,3,1}, {0,2,3}, {2,1,3}};
//unsigned int CFmtConverter::nodepos[4][3] = {{0,1,2}, {0,1,3}, {0,2,3}, {1,2,3}};

/* For each new triangle encountered, a unique id is given to it. This is a
 * number thats sequentially incremented.
 */
unsigned int CFmtConverter::trcnt = 1;


CFmtConverter::CFmtConverter(CFileReader *rdr) : m_rdr(rdr) {};

void CFmtConverter::Read(const char* fname, int type)
{
	if (type == 3)
	{
		m_rdr->ReadData3D(fname, m_vertlist, m_tetlist);	
		Type = type;		
		return;
		
	}
	m_rdr->ReadData2D(fname, m_vertlist, m_trilist);
	Type = type;
	
	
}

void CFmtConverter::ListTriFnexts()
{
	int i = 0;
	unsigned int v[3] = {0,0,0};
	int n = m_trilist.size();	
	for (int a = 0; a < n; a = a+3)
	{
		i += 1;
		v[0] = m_trilist[a];
		v[1] = m_trilist[a+1];
		v[2] = m_trilist[a+2];
		sort(v, v + 3);
                Tri_Info.insert(pair<int, int>(i, v[0]));
                Tri_Info.insert(pair<int, int>(i, v[1]));
                Tri_Info.insert(pair<int, int>(i, v[2]));
	}
}
/*
 * Given the value of a node, find its index in the triangle whose vertices are 
 * given by verts[3]
 */
unsigned int CFmtConverter::Index(unsigned int val, 
				const unsigned int verts[3])
{
	int idx = -1;
	if(verts[0] == val) idx = 0;
	else
	if(verts[1] == val) idx = 1;
	else
	if(verts[2] == val) idx = 2;
	
	assert(idx != -1);

	return idx;
}

/*
 * Given the values of the nodes of an edge, find its position in  tri-star
 */
unsigned int CFmtConverter::Pos(unsigned int val1, unsigned int val2, 
					const unsigned int verts[3])
{
	unsigned int idx1 = Index(val1, verts);
	unsigned int idx2 = Index(val2, verts);
	assert(idx1 != idx2);
	return edgepos[idx1][idx2];
}

/*
 * Lexicographically sort the tets and triangles. Useful while debugging
 */
void CFmtConverter::SortTetTri()
{
	std::vector<Tri> trivec;
	trivec.clear();
	std::vector<Tet>::iterator it = m_tetlist.begin();
	unsigned int tri[4][3];

	for(; it != m_tetlist.end(); it++)
	{
		SplitToTri(*it, tri);
		trivec.push_back(Tri(tri[0]));
		trivec.push_back(Tri(tri[1]));
		trivec.push_back(Tri(tri[2]));
		trivec.push_back(Tri(tri[3]));

		std::sort(it->node, it->node+4);
	}

	std::vector<Tri>::iterator itt = trivec.begin();
	std::sort(trivec.begin(), trivec.end());
	




	std::sort(m_tetlist.begin(), m_tetlist.end());


	


}

/*
 * Form a map which stores the fnext of each triangle
 */
void CFmtConverter::ProcessTets()
{
	unsigned int tri[4][3];
	std::vector<Tet>::iterator it = m_tetlist.begin();
	for(; it != m_tetlist.end(); it++)
	{
		//Store the nodes of a tet in oriented fashion
		OrientTet(*it);
		//Find the triangles in the tet
		SplitToTri(*it, tri);
		//Set the fnexts
		SetInnerFNexts(*it, tri);
	}

	std::map<Tri, TriInfo>::iterator itt = trimap.begin();	
	
	for(; itt != trimap.end(); itt++)
	{
		//For those trianlges on the boundary, fill fnext
		SetOuterFNexts(itt);
	}
}

/*
 * Generate 4 oriented triangles from a tet as given by nodepos
 */
void CFmtConverter::SplitToTri(const Tet & tet, unsigned int tri[4][3])
{
	for(unsigned int i = 0; i < 4; i++)
	{
		for(unsigned int j = 0; j < 3; j++)
			tri[i][j] = tet.node[nodepos[i][j]];

		std::sort(tri[i], tri[i]+3);
	}
}

/*
 * If (a > b) Swap(a,b)
 */
inline void CFmtConverter::Order(unsigned int & a, unsigned int & b)
{
	if(a > b)
	{
		unsigned int t = a;
		a = b;
		b = t;
	}
}

/*
 * For each triangle in a tet, 3 fnext fields can be set
 */
void CFmtConverter::SetInnerFNexts(const Tet & tet,  unsigned int tri[4][3])
{
	TriInfo* trinfo[4];
	for(unsigned int i = 0; i < 4; i++)
	{
		//assign a unque id for each new triangle
		trinfo[i] = &trimap[Tri(tri[i])];
		if(!trinfo[i]->id)
		{
			trinfo[i]->id = trcnt;
			trcnt++;
		}
	}
	for(unsigned int cur = 0; cur < 4; cur++)
	{
		unsigned int fnext[6] = {0,0,0,0,0,0};

		for(unsigned int j = 0; j < 3; j++)
		{
			//for each of the 3 edges of current triangle
			//find its position in triangle star
			unsigned int src = tet.node[nodepos[cur][j]];
			unsigned int des = tet.node[nodepos[cur][(j+1)%3]];
			unsigned int srctred = Pos(src, des, tri[cur]);

			/*
			 * find which of the 3 adjacent triangles is
			 * the fnext (using the vertex not present in
			 * the edge). find the position of the current 
			 * edge in the adjacent triangle (fnext)
			 */
			unsigned int nxt = fnextpos[cur][(j+2)%3];
			//unsigned int destred = Pos(src, des, tri[nxt]);
			//unsigned int edgenum = EdFacet(trinfo[nxt]->id, destred);
			assert(!fnext[srctred]);
			fnext[srctred] = trinfo[nxt]->id;
		}
		trinfo[cur]->Set(fnext);
	}
}
/*
 * For triangles that have unfilled Fnexts (triangles on the boundary, 
 * fill it by using the fact that an edge of a triangle on boundary
 * will be shared with another boundary triangle and they will be
 * fnext of each other
 */
void CFmtConverter::SetOuterFNexts(const std::map<Tri, TriInfo>::iterator & it)
{
	unsigned int pos[3];
	unsigned int src, des, ed[2];
	EdgeInfo* edinfo[3];

	//pos will be filled with the unfilled edge position
	//either 1,3,5 or 0,2,4 will be the unfilled edges
	if(!it->second.Filled(pos))
	{
		for(unsigned int i = 0; i < 3; i++)
		{
			//Find the unfilled edge
			ed[0] = src = it->first.node[posedge[pos[i]][0]];
			ed[1] = des = it->first.node[posedge[pos[i]][1]];
			Order(ed[0], ed[1]);

			//Check if we have seen the edge before
			edinfo[i] = &edgemap[Edge(ed)];
			unsigned int id = it->second.id; 
			if(!edinfo[i]->trid)
			{
				/*
				 * we are seeing this edge for the first time
				 * So just store the details of this edge and 
				 * corresponding triangle, so that when we see
				 * the edge again,with another triangle, we can 
				 * set the fnext to this one
				 */
				*edinfo[i] = EdgeInfo(id, pos[i], it);
			}
			else
			{
				TriInfo* trinf = &edinfo[i]->it->second;
				const unsigned int *tri = 
						edinfo[i]->it->first.node;
				unsigned fnext[6] = {0,0,0,0,0,0};
				unsigned int tristed;

				/*
				 * For the edge we have seen the second time,
				 * find its position relative to the earlier
				 * boundary triangle with which this edge
				 * is shared. This position of the edge 
				 * on the earlier seen triangle is current 
				 * unfilled positions fnext
				 */
				unsigned int v = Pos(src, des, tri);
				//fnext[pos[i]] = EdFacet(trinf->id, v);
				fnext[pos[i]] = trinf->id;
				it->second.Set(fnext);
				fnext[pos[i]] = 0;

				/*
				 * The fnext for the same edge in the earlier
				 * seen boundary triangle needs to be set.
				 * However the direction of the edge changes
				 * since the turn is now in the opp direction.
				 * Basically, find the directed edge 
				 * corresponding to the vacant position in the
				 * earlier triangle using posedge[v][0-1]
				 * Find the position of this directed edge
				 * in the triangle we have just seen, which
				 * will be the fnext
				 */
				v = edinfo[i]->v;
				src = tri[posedge[v][0]];
				des = tri[posedge[v][1]];
				tristed = Pos(src, des, it->first.node);
				//fnext[v] = EdFacet(it->second.id, tristed);
				fnext[v] = it->second.id;
				trinf->Set(fnext); 
			}
		}
	}
}

/*
 * Orient the vertices. If a b c d are the vertices of the tet
 * Find the normal of triangle abc by taking cross product
 * of ab and bc and check if the normal and ad are in the 
 * same direction (< 90 deg) by looking at the dot product
 */
void CFmtConverter::OrientTet(Tet & tet)
{
	my_float* u1 = m_vertlist[tet.node[0] - 1].xyz; 
	my_float* u2 = m_vertlist[tet.node[1] - 1].xyz; 

	my_float* v1 = m_vertlist[tet.node[1] - 1].xyz; 
	my_float* v2 = m_vertlist[tet.node[2] - 1].xyz; 

	my_float u[3];
	Sub(u2, u1, u);

	my_float v[3];
	Sub(v2, v1, v);

	my_float n[3];
	Cross(u,v,n);

	my_float* w1 = m_vertlist[tet.node[0] - 1].xyz; 
	my_float* w2 = m_vertlist[tet.node[3] - 1].xyz; 

	my_float w[3];
	Sub(w2, w1, w);

	if(Dot(n,w) < 0)
	{
		unsigned int v = tet.node[0]; 
		tet.node[0] = tet.node[2];
		tet.node[2] = v;
	}
}

void CFmtConverter::List()
{


	std::map<Tri, TriInfo>::iterator it = trimap.begin();
	unsigned int sz = trimap.size();
		
	for(; it != trimap.end(); it++)
	{		
		printf("(%d %d %d) -> %d : %d %d %d %d %d %d\n", 
		it->first.node[0], it->first.node[1], it->first.node[2],
		it->second.id, 
		it->second.fnext[0],
		it->second.fnext[1],
		it->second.fnext[2],
		it->second.fnext[3],
		it->second.fnext[4],
		it->second.fnext[5]);

		assert(
		it->second.fnext[0] && 
		it->second.fnext[1] && 
		it->second.fnext[2] && 
		it->second.fnext[3] && 
		it->second.fnext[4] && 
		it->second.fnext[5]);
	}
	
	printf("%d %d\n",sz, trcnt-1);	
}

/*
 * Fill the triangle star data structure by giving the
 * fnext for each triangle
 */
CTrist* CFmtConverter::CreateTrist(int batch)
{
	if (Type != 2)
	{
		ProcessTets();
		std::map<Tri, TriInfo>::iterator it = trimap.begin();
		CTrist* trist = new CTrist;
		trist->Allocate(m_vertlist.size(), trcnt - 1, batch);

		for(; it != trimap.end(); it++)
		{
			trist->MakeTri(it->first.node, it->second.id, it->second.fnext, Type);
		}
		
		return trist;
	}
	else
	{
		CTrist* trist = new CTrist;
                trist->Allocate(m_vertlist.size(),(int)m_trilist.size()/3, batch);
		return trist;
	}
}

void CFmtConverter::CopyVertexList(std::vector<Vertex> & vertexlist)
{	
	vertexlist = m_vertlist;	
}

void CFmtConverter::CopyVertexTriList(std::vector<Vertex> & vertexlist, std::vector<int> & trilist)
{
        vertexlist = m_vertlist;
	trilist = m_trilist;
}
