/*
 *	Copyright (C) 2010 Visualization & Graphics Lab (VGL), Indian Institute of Science
 *
 *	This file is part of libRG, a library to compute Reeb graphs.
 *
 *	libRG is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Lesser General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	libRG is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Lesser General Public License for more details.
 *
 *	You should have received a copy of the GNU Lesser General Public License
 *	along with libRG.  If not, see <http://www.gnu.org/licenses/>.
 *
 *	Author(s):	Aneesh Sood
 *	Version	 :	1.0
 *
 *	Modified by : Aneesh Sood.
 *	Date : 14th May 2011.
 *	Changes  : Added code to clear the contents of all containers at the beginning of 
 *  		   the execution. 
 */
 
#define MINIMUM  0
#define MAXIMUM  1
#define SADDLE  2

#include "CTrist.hpp"
#include <stdio.h>
#include <assert.h>
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string.h>
#include "CFmtConverter.hpp"
#include <map>
#include <set>
#include <unordered_map>
#include <sstream>
#include <stdlib.h>
#include <list>
#include <sys/time.h>

#include <bitset>

using namespace std;

using namespace __gnu_cxx;


unsigned int CTrist::org_max = 0x7FFFFFFF;
unsigned int CTrist::vo[6] = { 0, 1, 1, 2, 2, 0 };  /* origins wrt triangle version */
unsigned int CTrist::ve[6] = { 2, 5, 4, 1, 0, 3 };  /* enexts  ... */
unsigned int maxTriangle = 0;
int dimType = 0;
bool is3D = false;
int batchof = 0;
int CPdifference = 0;

struct timeval tv;

int* counterUp = NULL;
int* counterLow = NULL;


typedef std::multimap<int, int> map1;
typedef std::unordered_multimap<int, int> map_type;

map_type checkEdge;
vector<Vertex> vertex_list;
vector<int> tri_list;

vector<size_t> critical_Points;

vector<size_t> critical_Value;


std::list<int> triQ;

std::list<int> triHash;

std::list<int> triCompHash;


bool comp(int x, int y)
{
	
	
	if ((vertex_list[x - 1].xyz[3] < vertex_list[y - 1].xyz[3]) ||((vertex_list[x - 1].xyz[3] == vertex_list[y - 1].xyz[3]) && (x  <= y)))
	{

		return(true);
	}
	else
	{
	
		return(false);
	}
	
}
bool comparison(int x, int y)
{
        
	
	if ((vertex_list[x - 1].xyz[3] < vertex_list[y - 1].xyz[3]) ||((vertex_list[x - 1].xyz[3] == vertex_list[y - 1].xyz[3]) && (x  < y)))
        {
                return(true);
        }
        else
        {
                return(false);
        }

}

bool compUp(int x, int y)
{


        if ((vertex_list[x - 1].xyz[3] > vertex_list[y - 1].xyz[3]) ||((vertex_list[x - 1].xyz[3] == vertex_list[y - 1].xyz[3])))
        {
                return(true);
        }
        else
        {
                return(false);
        }

}
bool comparisonUp(int x, int y)
{


        if ((vertex_list[x - 1].xyz[3] > vertex_list[y - 1].xyz[3]) ||((vertex_list[x - 1].xyz[3] == vertex_list[y - 1].xyz[3]) && (x  > y)))
        {
                return(true);
        }
        else
        {
                return(false);
        }
}


std::multimap<int, int> Triangle_Info;

//std::map<int, int> Tri_Value;
std::vector<int> Tri_Value;


bool compare(int x, int y)
{


	if ((vertex_list[Tri_Value[x] - 1].xyz[3] > vertex_list[Tri_Value[y] - 1].xyz[3]) ||
	((vertex_list[Tri_Value[x] - 1].xyz[3] == vertex_list[Tri_Value[y] - 1].xyz[3]) && (Tri_Value[x]  >= Tri_Value[y])))
        {
                return(true);
        }
        else
        {
                return(false);
        }
}




//std::map<int, int> Tri_Value_Min;
std::vector<int> Tri_Value_Min;

std::map<int, int> Critical_Pointer;

std::map<int, int> Component;

std::multimap<int, int> Critical_Connection;

std::multimap<int, int> Edge_Adjacency;

std::multimap<int, int>* Critical_Path;

void CTrist::Clear()
{
	m_trist->last_triangle = 0;
	m_trist->used_triangles = 0;
}

void CTrist::Declare()
{
	is3D = true;
	dimType = 3;
}

bool lookup(const map_type& Map, int str, int value)
{
  pair<map_type::const_iterator, map_type::const_iterator> p =
    Map.equal_range(str);
  for (map_type::const_iterator i = p.first; i != p.second; ++i)
    {
	if ((*i).second == value)
	{
		return true;
	}	
    }
    return false;
}


void CTrist::Allocate(unsigned int max_n, unsigned int m, int bat)
{
	m_trist = new Trist;

	if (max_n > org_max)
	printf("trist_alloc: vertex range to big, %d > %d", max_n, org_max);
	m_trist->max_org = max_n;
	m_trist->max_triangle = m;
	maxTriangle = m;

	batchof = bat;
	m_tr = new Trist_record[m + 1];
	m_trist->triangle = m_tr;

	Clear();
}


unsigned int CTrist::MakeTri(const unsigned int node[3], unsigned int id, unsigned int *fnext, int type)
{
	unsigned int v, t;
	m_trist->last_triangle ++;
	t = id ? id : m_trist->last_triangle;
	
	assert(t <= m_trist->max_triangle);
	m_trist->used_triangles ++;
	m_tr[t].origin[0] = (Trist_org) node[0];
	m_tr[t].origin[1] = (Trist_org) node[1];
	m_tr[t].origin[2] = (Trist_org) node[2];
	dimType = 6;
	for(v = 0; v <= 5 ; v++)	
	m_tr[t].fnext[v] = fnext ? fnext[v] : t;
	return (t);
}

CTrist::CTrist(CFmtConverter *rdr) : reader(rdr) {};

void CTrist::CopyTriMap(std::multimap<int, int> TriCopy)
{
	Triangle_Info = TriCopy;	
}

void clear_all_Containers()
{	
	vertex_list.clear();
	tri_list.clear();
	critical_Points.clear();
	critical_Value.clear();
	triQ.clear();
	triHash.clear();
	triCompHash.clear();
	Triangle_Info.clear();
	Tri_Value.clear();
	Tri_Value_Min.clear();
	Critical_Pointer.clear();
	Component.clear();
	Critical_Connection.clear();
	checkEdge.clear();
	Edge_Adjacency.clear();
}
void CTrist::List(char* filename)
{
	clear_all_Containers();

	if (is3D)
	{
		reader->CopyVertexTriList(vertex_list, tri_list);
	}
	else
	{
		reader->CopyVertexList(vertex_list);
	}

	std::map<int, int> TriContainingRoot[vertex_list.size() + 1];
	std::map<int, int> upper[vertex_list.size() + 3];
	std::map<int, int> Critical_Upper[vertex_list.size() + 1];
	std::map<int, int> lower[vertex_list.size() + 3];
	std::map<int, int> Critical_Lower[vertex_list.size() + 1];
	std::map<int, int> Up_Star[vertex_list.size() + 1];
	counterUp = new int[vertex_list.size() + 1];
	counterLow = new int[vertex_list.size() + 1];
	for (int in = 0; in < (int)vertex_list.size() + 1;in++)
	{
		counterUp[in] = 0;
		counterLow[in] = 0;
	}
	Tri_Value.push_back(0);
	Tri_Value_Min.push_back(0);
	if (!(is3D))
	{
	
	for(unsigned int i = 1; i <=maxTriangle; i++)
	{
		Print(i, TriContainingRoot, vertex_list.size() + 1, upper, vertex_list.size() + 3, Critical_Upper, vertex_list.size() + 1, 
		Critical_Lower, vertex_list.size() + 1, lower, vertex_list.size() + 3, Up_Star, vertex_list.size() + 1);		
	}
	
	}

	else
	{
		Print2D(TriContainingRoot, vertex_list.size() + 1, upper, vertex_list.size() + 3, Critical_Upper, vertex_list.size() + 1,
		                Critical_Lower, vertex_list.size() + 1, lower, vertex_list.size() + 3, Up_Star, vertex_list.size() + 1);
		
	}
	for (int g = 1; g< (int)vertex_list.size() + 1;g++)
        {
               if (((counterUp[g] == counterLow[g]) && (counterUp[g] != 1 && counterLow[g] != 1) && (counterUp[g] != 0 && counterLow[g] != 0)) || (counterUp[g] != counterLow[g]))
	       {
	       		size_t cnt = g;
               		critical_Points.push_back(cnt);
	       } 
	}
	
	sort(critical_Points.begin(), critical_Points.end(), comp);
				
	for (int g = 0; g < (int)critical_Points.size(); g++)
	{
		int g1 = critical_Points[g];
		if (counterLow[g1] == 0)
		{
			size_t var = MINIMUM;
			critical_Value.push_back(var);
		}
		else
		{
			if (counterUp[g1] == 0)
			{
				size_t var = MAXIMUM;
				critical_Value.push_back(var);
			}
			else
			{
				size_t var = SADDLE;
				critical_Value.push_back(var);
			}
		}
        }
				
	std::map<int, int>::iterator it;
//	for(int g = 0; g < (int)critical_Points.size(); g++)
//	{
//		cout << g << " " << "Vertex " << critical_Points[g] << " Type " << critical_Value[g]  << " CU " << counterUp[critical_Points[g]] << " CL "<< counterLow[critical_Points[g]] << endl;
//	}
	
	Critical_Path = new std::multimap<int, int>[critical_Points.size() + 1];
	int loop = (int)(critical_Points.size()/batchof);
	int cur = 0;
	while (cur < loop)
	{
		
		ComputeReebgraph(Critical_Lower,vertex_list.size()+1, TriContainingRoot, vertex_list.size() + 1, Critical_Upper,vertex_list.size()+1, Up_Star, vertex_list.size() + 1, cur, batchof);
		
	cur++;
	}
	int difference = (int)critical_Points.size() - (cur*batchof);
	
	if (difference != 0)
	{
		ComputeReebgraph(Critical_Lower,vertex_list.size()+1, TriContainingRoot, vertex_list.size() + 1, Critical_Upper,vertex_list.size()+1, Up_Star, vertex_list.size() + 1, cur, difference);
	}
	cout << "Critical Points removed: " << CPdifference << endl;
	Output(filename);
	delete []counterUp;
	delete []counterLow;
	delete []Critical_Path;
	delete m_trist;
	delete []m_tr;	
}

void CTrist::Output(char* filename)
{
	ofstream out(filename);
	if (!out)
	{
		cout << "Cannot open file.\n";
		return; 
	}

	out << (int)critical_Points.size() - CPdifference << " " << (int)Critical_Connection.size() << endl;
        for(int g = 0; g < (int)critical_Points.size(); g++)
        {
		if ((int)critical_Points[g] != -1)
		{
                	out << critical_Points[g] - 1 << " ";
			out << vertex_list[critical_Points[g] - 1].xyz[3] << " ";
			if (critical_Value[g] ==0)
			{
				out << "MINIMA";
			}
			else
			{
				if (critical_Value[g] == 2)
				{
					out << "SADDLE" ;
				}
				else
				{
					out << "MAXIMA" ;
				}
			}
			out << endl;
		}
        }

        std::map<int, int>::iterator itt;
        std::multimap<int, int>::iterator itt1;
        for (itt = Critical_Connection.begin(); itt!= Critical_Connection.end(); itt++)
        {
                out << critical_Points[(*itt).first] - 1 << " " << critical_Points[(*itt).second] -1  << " " << -1 << endl;
        }	
}

void CTrist::ComputeReebgraph(std::map<int, int>* Critical_Lower, size_t total2, std::map<int, int>* TriContainingRoot, size_t total, std::map<int, int>* Critical_Upper, size_t total1, std::map<int, int>* Up_Star, size_t total3, int cur, int batch)
{
	std::unordered_map<int, int> CriticalSetComplete[batch];
	ComputeCriticalSet(Critical_Lower, vertex_list.size() + 1, TriContainingRoot, vertex_list.size() + 1, CriticalSetComplete, batch, Critical_Upper, vertex_list.size() + 1, cur, batch);
//	cout << "difference " << sizebefore - sizeafter << endl;

	ConnectCriticalPoints(TriContainingRoot, vertex_list.size() + 1, Critical_Upper, vertex_list.size() + 1, Critical_Lower, vertex_list.size() + 1, Up_Star, vertex_list.size() + 1, CriticalSetComplete, batch, cur, batch);
}

/*ASCII printout of triangle record t to given file.*/
void CTrist::Print(int e, std::map<int, int>* TriContainingRoot, size_t total1, std::map<int, int>* upper, size_t total, 
		std::map<int, int>* Critical_Upper, size_t total4, std::map<int, int>* Critical_Lower, size_t total5, std::map<int, int>* lower, size_t total2, std::map<int, int>* Up_Star, size_t total3)
{
	unsigned int i, j, k, t;
	t = e;
	i = m_tr[t].origin[0];
	j = m_tr[t].origin[1];
	k = m_tr[t].origin[2];
	int temp[3] = {i,j,k};	
	sort(temp, temp+3, comp);
	Tri_Value_Min.push_back(temp[0]);
	Tri_Value.push_back(temp[2]);
	Up_Star[temp[0]][temp[2]] = t;
	Up_Star[temp[1]][temp[2]] = t;
	
	Traverse(temp[0],temp[1],temp[2],t, Critical_Upper, total4, Critical_Lower, total5, TriContainingRoot, vertex_list.size() + 1, upper, total1, lower, total2);	
	//printf("(%d %d %d) -> %d : ", i, j, k, t);
	//for(int v = 0; v < dimType; v++)
	//printf("%d ", m_tr[t].fnext[v]);
	//printf("\n");

}

void CTrist::Print2D(std::map<int, int>* TriContainingRoot, size_t total1, std::map<int, int>* upper, size_t total,std::map<int, int>* Critical_Upper, size_t total4, 
				std::map<int, int>* Critical_Lower, size_t total5, std::map<int, int>* lower, size_t total2, std::map<int, int>* Up_Star, size_t total3)
{
        int n = tri_list.size();
	int i = 0;
	unsigned int v[3] = {0,0,0};
	int IdentityNo = 1;
	std::map<int, int> trace[n/3 + 1];
	std::map<int, int> EdgeID[vertex_list.size() + 1];	
        for (int a = 0; a < n; a = a+3)
        {
		i += 1;
		v[0] = tri_list[a];
                v[1] = tri_list[a+1];
                v[2] = tri_list[a+2];
                sort(v, v+ 3);
		m_tr[i].origin[0] = v[0];
		m_tr[i].origin[1] = v[1];
		m_tr[i].origin[2] = v[2];
		
		m_tr[i].fnext[0] = 0;
		m_tr[i].fnext[1] = 0;
		m_tr[i].fnext[2] = 0;
		if (!(EdgeID[v[0]][v[1]]))
                {
			EdgeID[v[0]][v[1]] = IdentityNo;
			Edge_Adjacency.insert(pair<int, int>(IdentityNo, i));
			trace[i][IdentityNo] = 0;
			IdentityNo += 1;
                }
                else
                {
			Edge_Adjacency.insert(pair<int, int>(EdgeID[v[0]][v[1]], i));
			Fill(v[0],v[1],i, 0, trace, n/3, EdgeID, vertex_list.size() + 1);
		}
		if (!(EdgeID[v[1]][v[2]]))
		{
			EdgeID[v[1]][v[2]] = IdentityNo;
			Edge_Adjacency.insert(pair<int, int>(IdentityNo, i));
			trace[i][IdentityNo] = 1;
			IdentityNo += 1;
		}
		else
		{
			Edge_Adjacency.insert(pair<int, int>(EdgeID[v[1]][v[2]], i));
			Fill(v[1],v[2],i, 1, trace, n/3, EdgeID, vertex_list.size() + 1);
		}
		if (!(EdgeID[v[0]][v[2]]))
		{
			EdgeID[v[0]][v[2]] = IdentityNo;
			Edge_Adjacency.insert(pair<int, int>(IdentityNo, i));
			trace[i][IdentityNo] = 2;
			IdentityNo += 1;
		}
		else
		{
			Edge_Adjacency.insert(pair<int, int>(EdgeID[v[0]][v[2]], i));
			Fill(v[0],v[2],i, 2, trace, n/3, EdgeID, vertex_list.size() + 1);
		}
		sort(v, v+3, comp);
		Tri_Value.push_back(v[2]);
		Tri_Value_Min.push_back(v[0]);
		Up_Star[v[0]][v[2]] = i;
		Up_Star[v[1]][v[2]] = i;
		Traverse(v[0],v[1],v[2],i, Critical_Upper, total4, Critical_Lower, total5, TriContainingRoot, vertex_list.size() + 1, upper, total1, lower, total2);
		//printf("(%d %d %d) -> %d : ", i, j, k, t);
		//for(v = 0; v < dimType; v++)
		
		//printf("%d ", m_tr[t].fnext[v]);
		//printf("\n");
	}
	//for (int t1 = 1; t1 < (int)(tri_list.size())/3 + 1; t1++)
	//{
	//	cout << t1 << " -> " << m_tr[t1].fnext[0] << " " << m_tr[t1].fnext[1] << " " << m_tr[t1].fnext[2] << endl;
	//}
}

void CTrist::Fill(int v, int u, int i, int w, std::map<int, int>* trace, size_t space, std::map<int, int>* EdgeID, size_t total)
{
	std::multimap<int, int>::iterator itt;
	itt = Edge_Adjacency.equal_range(EdgeID[v][u]).first;
	m_tr[i].fnext[w] = ((*itt).second);
	m_tr[(*itt).second].fnext[trace[(*itt).second][EdgeID[v][u]]] = (i);
}

void CTrist::Traverse(int i1, int j1, int k1, int t, std::map<int, int>* Critical_Upper, size_t total3, std::map<int, int>* Critical_Lower, size_t total4, 
			std::map<int, int>* TriContainingRoot, size_t total, std::map<int, int>* upper, size_t total1, std::map<int, int>* lower, size_t total2)
{
	std::map<int, int>::iterator it;
	
	if (!(upper[j1][k1]))
	{
		upper[j1][k1] = -1;
		counterUp[j1] += 1;
		TriContainingRoot[j1][k1] = t;
		Critical_Upper[j1][k1] = k1;
	}
	if (!(lower[j1][i1]))
	{
		lower[j1][i1] = -1;
                counterLow[j1] += 1;
                TriContainingRoot[j1][i1] = t;
                Critical_Lower[j1][i1] = i1;
	}

		if (!(upper[i1][k1]))
		{
			upper[i1][k1] = 0;
			counterUp[i1] += 1;
			TriContainingRoot[i1][k1] = t;
                        Critical_Upper[i1][k1] = k1;
		}
		if (!(upper[i1][j1]))
		{
			if (upper[i1][k1] <= 0)
			{
				upper[i1][j1] = k1;
				upper[i1][k1] -= 1;
				//TriContainingRoot[i1][k1] = t;
				//Critical_Upper[count][j1] = j1;
			}
			else
			{
				int root = Find(1, i1, k1, upper, total1, lower, total2);
				upper[i1][j1] = root;
				upper[i1][root] -= 1;
				TriContainingRoot[i1][root] = t;
			}
		}
		else
		{
			if (upper[i1][j1] <= 0)
			{
				int root = 0;
				if (upper[i1][k1] > 0)
				{
					root = Find(1, i1, k1, upper, total1, lower, total2);
				}
				else
				{
					root = k1;
				}
				if (upper[i1][j1] <= upper[i1][root] && (j1 != root))
				{
					upper[i1][root] = j1;
					upper[i1][j1] -= 1;
					counterUp[i1] -= 1;
					TriContainingRoot[i1][j1] = t;
					it = Critical_Upper[i1].find(root);
					Critical_Upper[i1].erase(it);
				}
				else
				{
					if (j1 != root)
					{
						upper[i1][j1] = root;
						upper[i1][root] -= 1;
						counterUp[i1] -= 1;
						TriContainingRoot[i1][root] = t;
						it = Critical_Upper[i1].find(j1);
						Critical_Upper[i1].erase(it);		
					}
				}
			}
			else
			{
				int root1 = 0, root2 = 0;
				root1 = Find(1, i1, j1, upper, total1, lower, total2);
				if (upper[i1][k1] > 0)
				{
					root2 = Find(1, i1, k1, upper, total1, lower, total2);
				}
				else
				{
					root2 = k1;
				}
				if (upper[i1][root1] <= upper[i1][root2] && (root1 != root2))
				{
					upper[i1][root2] = root1;
					upper[i1][root1] -= 1;
					counterUp[i1] -= 1;
					TriContainingRoot[i1][root1] = t;
					it = Critical_Upper[i1].find(root2);		
					Critical_Upper[i1].erase(it);
				}
				else
				{
					if (root1 != root2)
					{
						upper[i1][root1] = root2;
						upper[i1][root2] -= 1;
						counterUp[i1] -= 1;
						TriContainingRoot[i1][root2] = t;
						it = Critical_Upper[i1].find(root1);
						Critical_Upper[i1].erase(it);
					}
				}
			}		
		}

		int tem = k1;
		k1 = i1;
		i1 = tem;

		if (!(lower[i1][k1]))
                {
                       	lower[i1][k1] = 0;
			counterLow[i1] += 1;
			TriContainingRoot[i1][k1] = t;
			Critical_Lower[i1][k1] = k1;
		}
                if (!(lower[i1][j1]))
		{
			if (lower[i1][k1] <= 0)
			{
				lower[i1][j1] = k1;
				lower[i1][k1] -= 1;
				//iContainingRoot[i1][k1] = t;
				//itical_Upper[count][j1] = j1;
			}
			else
			{
				int root = Find(2, i1, k1, upper, total1, lower, total2);
				lower[i1][j1] = root;
				lower[i1][root] -= 1;
				TriContainingRoot[i1][root] = t;
			}
		}
		else
		{
			if (lower[i1][j1] <= 0)
			{
				int root = 0;
				if (lower[i1][k1] > 0)
				{
					root = Find(2, i1, k1, upper, total1, lower, total2);
				}
				else
				{
					root = k1;
				}
				if (lower[i1][j1] <= lower[i1][root] && (j1 != root))
				{
					lower[i1][root] = j1;
					lower[i1][j1] -= 1;
					counterLow[i1] -= 1;
					TriContainingRoot[i1][j1] = t;
					it = Critical_Lower[i1].find(root);
					Critical_Lower[i1].erase(it);
				}
				else
				{
					if (j1 != root)
					{
						lower[i1][j1] = root;
						lower[i1][root] -= 1;
						counterLow[i1] -= 1;
						TriContainingRoot[i1][root] = t;
						it = Critical_Lower[i1].find(j1);
						Critical_Lower[i1].erase(it);
					}
				}
			}
			else
			{
				int root1 = 0, root2 = 0;
				root1 = Find(2, i1, j1, upper, total1, lower, total2);
				if (lower[i1][k1] > 0)
				{
					root2 = Find(2, i1, k1, upper, total1, lower, total2);
				}
				else
				{
					root2 = k1;
				}
				if (lower[i1][root1] <= lower[i1][root2] && (root1 != root2))
				{
					lower[i1][root2] = root1;
					lower[i1][root1] -= 1;
					counterLow[i1] -= 1;
					TriContainingRoot[i1][root1] = t;
					it = Critical_Lower[i1].find(root2);
					Critical_Lower[i1].erase(it);
				}
				else
				{
					if (root1 != root2)
					{
						lower[i1][root1] = root2;
						lower[i1][root2] -= 1;
						counterLow[i1] -= 1;
						TriContainingRoot[i1][root2] = t;
						it = Critical_Lower[i1].find(root1);
						Critical_Lower[i1].erase(it);
					}
				}
			}
		}
	
}	

int CTrist::Find(int flag, int cv, int x, std::map<int, int>* upper, size_t total1, std::map<int, int>* lower, size_t total2)
{
	int n = 0;
	if (flag == 1)
	{
		int next = upper[cv][x];
		if (next > 0)
		{
			n = Find(1, cv, next, upper, total1, lower, total2);
			return(n);
		}
		else
		{
			return(x);
	}	}
	else
	{
		int next = lower[cv][x];
                if (next > 0)
                {
			n = Find(2, cv, next, upper, total1, lower, total2);
			return(n);			
                }
		else
		{
                	return(x);
		}
	}
}	

inline unsigned int CTrist::Sym(unsigned int e)
{
	return Odd(e) ? (e - 1) : (e + 1);
}

/* Returns Org (Sym (e)); abbrev: Dest (e). */
inline unsigned int CTrist::Dest (unsigned int e)
{
	unsigned int t = TrIndex (e), v = TrVersion (Sym(e));
	return (m_tr[t].origin[vo[v]] & org_Mask);
}


/* Returns Org(Enext2(Fnext(e))); abbrev: Apex (e) */
inline unsigned int CTrist::Apex(unsigned int e)
{
	unsigned int t = TrIndex (e), v = TrVersion (e);

	e = m_tr[t].fnext[v];
	t = TrIndex (e);
	v = TrVersion (e);
	return (m_tr[t].origin[vo[ve[ve[v]]]] &org_Mask);
}

/* Returns Enext (Fnext (Sym (Enext (e)))); abbrev: Turn (e). */
inline unsigned int CTrist::Turn(unsigned int e)
{
	unsigned int t = TrIndex (e), v = TrVersion (e);
	v = ve[v];
	v = Sym(v);
	e = m_tr[t].fnext[v];
	t = TrIndex (e);
	v = TrVersion (e);
	return (EdFacet (t, ve[v]));
}

/* Returns Enext (Enext (e)); abbrev: Enext2 (e). */
inline unsigned int CTrist::Enext2 (unsigned int e)
{
	unsigned int t = TrIndex (e), v = TrVersion (e);
	return (EdFacet (t, ve[ve[v]]));
}

/* Basic triangle-edge function: abbrev: Fnext (e). */
inline unsigned int CTrist::Fnext (unsigned int e)
{
	unsigned int t = TrIndex (e), v = TrVersion (e);
	return (m_tr[t].fnext[v]);
}

/* Basic triangle-edge function; abbrev: Enext (e). */
inline unsigned int CTrist::Enext (unsigned int e)
{
  unsigned int t = TrIndex (e), v = TrVersion (e);
  return (EdFacet (t, ve[v]));	
}

/* Basic triangle-edge function; abbrev: Org (e). */
inline unsigned int CTrist::Org (unsigned int e)
{
	unsigned int t = TrIndex (e), v = TrVersion (e);
	return (m_tr[t].origin[vo[v]] & org_Mask);
}

void CTrist::CheckConsistency()
{
	for(unsigned int i = 1; i <= m_trist->max_triangle; i++)
	{
		for(unsigned int v = 0; v < 6; v++)
		{
			unsigned int a = EdFacet(i,v);
			unsigned int b = Fnext(a);
			assert(Sym(Sym(a)) == a);
			assert(Enext(Enext2(a)) == a);
			assert(Enext((a)) != a);
			assert(Enext2((a)) != a);
			assert(Enext(Sym(a)) != a);
			assert(Enext2(Sym(a)) != a);
			assert(Enext(Enext2(Sym(a))) != a);
			assert(Enext(Sym(Enext(Sym(a)))) == a);
			while(a != b)
			{
				b = Fnext(b);
				//printf("a = %d != b = %d\n", a, b);
			}
			b = Fnext(Sym(a));
			//for all i; limit to 10
			for(int k = 0; k < 10; k++)
			{
				assert(b != a);
				b = Fnext(b);
			}
			assert(Fnext(Sym(Fnext(Sym(a)))) == a);
			assert(Org(a) != Org(Sym(a)));
			assert(Org(Enext(a)) == Org(Sym(a)));
			assert(Org(Enext2(a)) != Org(a));
			assert(Org(Fnext(a)) == Org(a));
		}
	}
}

void CTrist::ConnectCriticalPoints(std::map<int, int>* TriContainingRoot, size_t total, std::map<int, int>* Critical_Upper, size_t total1, std::map<int, int>* Critical_Lower, size_t total2, std::map<int, int>* Up_Star, size_t total3, std::unordered_map<int, int>* CriticalSetComplete, size_t total5, int cur, int batch)
{
	int triIndex;
	int triValue;
	bool flag = false;
	std::map<int, int>::iterator it;
	int levelNo;
	int extent = cur*batchof + batch;
//	******** deal with the queue here ********
	std::list<int>::iterator iter;
	std::list<int>::iterator iter1;
	std::list<int>::iterator iter2;
	iter1 = triHash.begin();
	iter = triQ.begin();
	iter2 = triCompHash.begin();
	while (iter != triQ.end())
	{
		if (*iter == -1)
		{
			iter++;
			iter1++;
			iter2++;
			continue;
		}
		flag = true;
		levelNo = *iter1;
		triIndex = *iter;
		triValue = TravelUp(TriContainingRoot, vertex_list.size() + 1, Critical_Upper, total, Critical_Lower, total2, triIndex, critical_Points[levelNo], levelNo, CriticalSetComplete, total5, Up_Star, total3, *iter2, flag, extent, cur);

		if (triValue != 0)
		{
			*iter = triValue;
		}
		else
		{
			*iter = -1;
			*iter1 = -1;
			*iter2 = -1;
		}
		iter++;
		iter1++;
		iter2++;
	}
	flag = false;
	levelNo = extent - batch;

	while(levelNo < extent)
        {
		
		if ((int)critical_Points[levelNo] == -1)
		{
			levelNo++;
			continue;
		}
		int component = 0;
		for (it = Critical_Upper[critical_Points[levelNo]].begin(); it != Critical_Upper[critical_Points[levelNo]].end(); it++)
		{
			component++;
			
			triIndex = TriContainingRoot[critical_Points[levelNo]][Critical_Upper[critical_Points[levelNo]][(*it).first]];

			triValue = TravelUp(TriContainingRoot, vertex_list.size() + 1, Critical_Upper, total, Critical_Lower, total2, triIndex, critical_Points[levelNo], levelNo, CriticalSetComplete, total5, Up_Star, total3, component, flag, extent, cur);

		}
		levelNo += 1;
	}
}

int CTrist::TravelUp(std::map<int, int>* TriContainingRoot, size_t total, std::map<int, int>* Critical_Upper, size_t total1,std::map<int, int>* Critical_Lower, size_t total2, int Index, int criticalPoint, int level, std::unordered_map<int, int>* CriticalSetComplete, size_t total3, std::map<int, int>* Up_Star, size_t total4, int component, bool flag1, int extent, int cur)
{
	std::map<int, int>::iterator rit;
	std::map<int, int>::iterator itta;
	int secondLevel;
	int offset = cur*batchof;
	if (flag1)
	{
		secondLevel = offset;
	}
	else
	{
		secondLevel = level + 1;
	}
	int nextTri;
	nextTri = Index;
	int currentPoint;
	int flag = 0;
	Component[criticalPoint] = 0;
	std::vector<bool> Tri_Tag(maxTriangle, false);
	while (secondLevel < extent)
	{
		if ((int)critical_Points[secondLevel] == -1)
		{
			secondLevel++;
			continue;
		}
		if (flag == 1)
		{
			currentPoint = Tri_Value[Index];
			if (Up_Star[currentPoint].empty())
			{
				break;
			}
			else
			{
				rit = Up_Star[currentPoint].begin();
//				itta = Up_star[currentPoint].begin();
				nextTri = rit->second;
//				while (Tri_Tag[nextTri])
//				{	
//					rit++;
//					nextTri = rit->second;
//					if (rit == Up_Star[currentPoint].rend())
//					{
//						cout << " not possible " << endl;
//					}
//					cout << nextTri << endl;
//				}
			}
          	}
		Critical_Path[level].insert(pair<int, int>(component, Index));

		if (comp(Tri_Value_Min[nextTri], critical_Points[secondLevel]) && comp(critical_Points[secondLevel], Tri_Value[nextTri]))
		{

			if (CriticalSetComplete[secondLevel - offset][nextTri])
			{
				bool p=	lookup(checkEdge, secondLevel, CriticalSetComplete[secondLevel - offset][nextTri]);
				if (!p)
				{				
					Critical_Connection.insert(pair<int, int>(level, secondLevel));
					checkEdge.insert(map_type::value_type(secondLevel, CriticalSetComplete[secondLevel - offset][nextTri]));
				}			
//				cout << "this is it " << criticalPoint << " and " << critical_Points[secondLevel] << endl;
				return 0;
			}

//			cout << "CP did not match : " << critical_Points[level] << " : " << critical_Points[secondLevel] << endl;
			secondLevel++;
			continue;
		}
		else
		{
			Index = nextTri;
			flag = 1;
		}
//		secondLevel++;
	}
//	********* add to queue and mark triangle here *********
	if (!flag1)
	{
		triQ.push_back(nextTri);
		triHash.push_back(level);
		triCompHash.push_back(component);
//		cout << "triangle " << nextTri <<"  " ;
	}
	
//	triHash.push_back(level);
//	cout << "CP " << triHash[nextTri] << " - " << level << endl;
//	triCompHash.insert(std::unordered_map<int,int>::value_type(nextTri, component));
	return nextTri;
}

void CTrist::CalculateCriticalSet(std::map<int, int>* TriContainingRoot, size_t total, std::map<int, int>* Critical_Upper, size_t total1, 
                                        std::map<int, int>* Critical_Lower, size_t total2, std::multimap<int, int>* CriticalSet, size_t total3)
{
	int levelNo = 0;
	int Component = 0;
	double value;	 
	std::map<int, int>::iterator it;
	while(levelNo != (int)critical_Points.size())
	{
		if (critical_Value[levelNo] == 0)
		{
			Component = 1;
			value = (double)vertex_list[critical_Points[levelNo] - 1].xyz[3] - 0.00000001; 
			//ComputeSet(Critical_Lower, total2, TriContainingRoot, total, critical_Points[levelNo], Component, value, CriticalSet, total3);
			//std::multimap<int, int>::iterator itt;
                        ////cout << "*********************" << critical_Points[levelNo] << endl;
			//for (itt = CriticalSet[critical_Points[levelNo]].begin(); itt != CriticalSet[critical_Points[levelNo]].end(); itt++)
                        //{
			//	//cout << (*itt).first << " -> " << (*itt).second << "(" <<vertex_list[Tri_Value_Min[(*itt).second] - 1].xyz[3] << " " << vertex_list[Tri_Value[(*itt).second] - 1].xyz[3] << ")" <<  " => ";
			//}
			////cout << "\n" << "*********************" << endl;
			Component = 0;
		}
		else
		{
			if (critical_Value[levelNo] == 1)
			{
				Component = 1;
	                        value = (double)vertex_list[critical_Points[levelNo] - 1].xyz[3] - 0.00000001;
				//ComputeSet(Critical_Lower, total2, TriContainingRoot, total, critical_Points[levelNo], Component, value, CriticalSet, total3);
				//std::multimap<int, int>::iterator itt;
	                        ////cout << "*********************" << critical_Points[levelNo] << endl;
				//for (itt = CriticalSet[critical_Points[levelNo]].begin(); itt != CriticalSet[critical_Points[levelNo]].end(); itt++)
	                        //{
	                        //       //cout << (*itt).first << " -> " << (*itt).second << "(" <<vertex_list[Tri_Value_Min[(*itt).second] - 1].xyz[3] << " " << vertex_list[Tri_Value[(*itt).second] - 1].xyz[3] << ")" <<  " => ";
	                        //}
	                        ////cout << "\n" << "*********************" << endl;
				Component = 0;
			}
			else
			{
				if (critical_Value[levelNo] == 2)
				{
					Component += 1;
					value = (double)vertex_list[critical_Points[levelNo] - 1].xyz[3] - 0.00000001;
					//ComputeSet(Critical_Lower, total2, TriContainingRoot, total, critical_Points[levelNo], Component, value, CriticalSet, total3);
					if (critical_Points[levelNo] == 191)
					{
					//std::multimap<int, int>::iterator itt;
	                                ////cout << "*********************" << critical_Points[levelNo] << endl;
	                                //for (itt = CriticalSet[critical_Points[levelNo]].begin(); itt != CriticalSet[critical_Points[levelNo]].end(); itt++)
	                                //{
					//	//cout << (*itt).first << " -> " << (*itt).second << "(" << vertex_list[Tri_Value_Min[(*itt).second] - 1].xyz[3] << " " << vertex_list[Tri_Value[(*itt).second] - 1].xyz[3] << ")" <<  " => ";
					//}
					
					////cout << "\n" << "*********************" << endl;
					}
					Component = 0;
				}
				//else
				//{
				//	for (it = Critical_Upper[critical_Points[levelNo]].begin(); it != Critical_Upper[critical_Points[levelNo]].end(); it++)
				//	{
				//		Component += 1;
		                //        	triIndex = TriContainingRoot[critical_Points[levelNo]][Critical_Upper[critical_Points[levelNo]][(*it).first]];		                        	
		                //        	value = (double)vertex_list[critical_Points[levelNo] - 1].xyz[3] + 0.00000001;
		                //        	ComputeSet(triIndex, critical_Points[levelNo], Component, value, CriticalSet, total3);
				//	}
		                        //std::multimap<int, int>::iterator itt;
		                        ////cout << "*********************" << critical_Points[levelNo] <<  endl;
					//for (itt = CriticalSet[critical_Points[levelNo]].begin(); itt != CriticalSet[critical_Points[levelNo]].end(); itt++)
		                        //{						
		                        //	 //cout << (*itt).second << "(" <<vertex_list[Tri_Value_Min[(*itt).second] - 1].xyz[3] << " " << vertex_list[Tri_Value[(*itt).second] - 1].xyz[3] << ")" <<  " => ";
					//}		                        
					////cout << "\n" << "*********************" << endl;
				//	Component = 0;
				//}
			}
		}
		levelNo += 1;
	}
}

bool CTrist::ComputeSet(std::vector<vector<bool> > &Tri_Tag,std::map<int, int>::iterator* cit, size_t number, int secondLevel, std::map<int, int>* Critical_Lower, size_t total2, std::map<int, int>* TriContainingRoot, size_t total, int triangle, int criticalPoint, int componentNo, double value, std::multimap<int, int>* CriticalSet, size_t total3)
{
	if (Critical_Lower[criticalPoint].empty())
	{
		return(false);
	}
	int triIndex;
	int Index = 1;
	bool found = false;	
	std::multimap<int, int>::iterator it;
	std::map<int, int>::iterator itt;
	itt = Critical_Lower[criticalPoint].begin();
	//cout<<"Size of Critical_Lower for "<<criticalPoint<<" is "<<Critical_Lower[criticalPoint].size()<<std::endl;
	for (; itt != Critical_Lower[criticalPoint].end(); itt++)
	{
		Component[criticalPoint] += 1;
		triIndex = TriContainingRoot[criticalPoint][Critical_Lower[criticalPoint][(*itt).first]];
		CriticalSet[secondLevel].insert(pair<int, int>(Component[criticalPoint], triIndex));
		if (CriticalSet[secondLevel].size() == 1)
		{
			cit[secondLevel] = CriticalSet[secondLevel].begin();
		}
		Tri_Tag[secondLevel][triIndex] = true;
		Index += 1;
		for (it = cit[secondLevel]; it != CriticalSet[secondLevel].end(); it++)
		{
			//cout<<"Size of cit for "<<secondLevel<<" is "<<cit->size()<<std::endl;
			triIndex = (*it).second;
			cit[secondLevel] = it;
			unsigned int i, j, k, v;
			bool flag1, flag2, flag3;
		        v = 0;
		        i = (m_tr[triIndex].origin[vo[v]] & org_Mask); v = ve[v];
		        j = (m_tr[triIndex].origin[vo[v]] & org_Mask); v = ve[v];
		        k = (m_tr[triIndex].origin[vo[v]] & org_Mask);
			flag1 = comparison(i, criticalPoint); flag2 = comparison(j, criticalPoint); flag3 = comparison(k, criticalPoint);
			int next[dimType - dimType/3];
			if (flag1 == flag2)
			{
				for (int a = 0; a < dimType - dimType/3; a++)
				{
					next[a] = a + dimType/3;
				}
			}
			if (flag1 == flag3)
                        {
                                for (int a = 0; a < dimType - dimType/3; a++)
                                {
                                        next[a] = a;
                                }
                        }
			if (flag2 == flag3)
			{
				if (dimType == 6)
				{
					next[0] = 0;next[1] = 1;next[2] = 4;next[3] = 5;
				}
				else
				{
					next[0] = 0;next[1] = 2;
				}
			}
			if (triIndex == triangle)
			{
				return(true);
			}
			else
			{
				for (int a = 0; a < dimType - dimType/3; a++)
                                {
                                        int nextTri = (m_tr[triIndex].fnext[next[a]]);
                                        if (comp(Tri_Value_Min[nextTri], criticalPoint) && comp(criticalPoint, Tri_Value[nextTri]))
					{
                                                if (nextTri == triangle)
						{
							found = true;
							return(true);
						}
						if(Tri_Tag[secondLevel][nextTri])
                                                {
                                                        continue;
                                                }
                                                else
                                                {
							CriticalSet[secondLevel].insert(pair<int, int>(Component[criticalPoint], nextTri));
                                                        Tri_Tag[secondLevel][nextTri] = true;
                                                        Index += 1;
                                                }
                                        }
                                }
			}			
		}		
	}
	return(false);
}

void CTrist::ComputeCriticalSet(std::map<int, int>* Critical_Lower, size_t total2, std::map<int, int>* TriContainingRoot, size_t total, std::unordered_map<int, int>* CriticalSetComplete, size_t total5, std::map<int, int>* Critical_Upper, size_t total1, int cur, int batch)
{
	int z = 0;
	int criticalPoint = 0;
	int triIndex;
	
	while (z < batch)
	{

		bool getout = false;
		int remove = 0;
		int component = 0;
		int offset = cur*batchof;
		criticalPoint = critical_Points[offset+z];

//		map1 innermap;
		std::list<int> innermap;
		innermap.clear();
		bool trueCP = true;
		std::vector<bool> Tag(maxTriangle, false);
		if (Critical_Lower[criticalPoint].size() == 1)
		{
//			getout = true;
			
			remove = ComputeCriticalSetUp(z+offset, Critical_Upper, total1, TriContainingRoot, total);

			if (remove == 1)
			{
				CPdifference++;
				z++;
				continue;
			}
		}
		std::map<int, int>::iterator itt;
	        itt = Critical_Lower[criticalPoint].begin();
		
	        
		for (; itt != Critical_Lower[criticalPoint].end(); itt++)
		{
			component++;
			set<int> queueCP;
			int left = (int)Critical_Lower[criticalPoint].size() - 1;
			triIndex = TriContainingRoot[criticalPoint][Critical_Lower[criticalPoint][(*itt).first]];
			
			innermap.push_back(triIndex);
//			std::stringstream sstr;
//                        sstr << triIndex;
//                        std::string str1 = sstr.str();
//                        long int key = strtol(str1.c_str(), NULL, 10);
			if (Tag[triIndex] == true)
			continue;
                        CriticalSetComplete[z][triIndex] = component;
				
			Tag[triIndex] = true;
			std::list<int>::iterator iter;
			for (iter = innermap.begin(); iter != innermap.end(); iter++)
			{
				triIndex = (*iter);
				unsigned int i, j, k, v;
                      		bool flag1, flag2, flag3;
				int subs = dimType/3;
                        	v = 0;
                        	i = (m_tr[triIndex].origin[vo[v]] & org_Mask); v = ve[v];
                        	j = (m_tr[triIndex].origin[vo[v]] & org_Mask); v = ve[v];
                        	k = (m_tr[triIndex].origin[vo[v]] & org_Mask);
				flag1 = comparison(i, criticalPoint); flag2 = comparison(j, criticalPoint); flag3 = comparison(k, criticalPoint);
				int next[dimType - subs];
				if (flag1 == flag2)
				{
                       	        	for (int a = 0; a < dimType - subs; a++)
                       	        	{
                                	        next[a] = a + subs;
                                	}
                        	}
                        	if (flag1 == flag3)
                        	{
                        	        for (int a = 0; a < dimType - subs; a++)
                       	        	{
                                	        next[a] = a;
                                	}
                        	}
                        	if (flag2 == flag3)
                        	{
					int a = 0; int a1 = 0;
					while (a < subs)
					{
						next[a1] = a;
						a++; a1++;
					}
					a = a + subs;
					while (a < dimType)
					{
						next[a1] = a;
						a++; a1++;
					}
                        	}
				for (int a = 0; a < dimType - subs; a++)
                                {
                                        int nextTri = (m_tr[triIndex].fnext[next[a]]);	
                                        if (comp(Tri_Value_Min[nextTri], criticalPoint) && comp(criticalPoint, Tri_Value[nextTri]))
					{
						if (1)
						{
							std::map<int, int>::iterator inneritt;
							inneritt = itt;
							inneritt++;
							int count = component;
							for (; inneritt != Critical_Lower[criticalPoint].end(); inneritt++)
							{
								count++; 
							if ( TriContainingRoot[criticalPoint][Critical_Lower[criticalPoint][(*inneritt).first]] == nextTri)
							{
								if(queueCP.find(nextTri) == queueCP.end())
								{	
									
//									Critical_Lower[criticalPoint].erase(inneritt);
									queueCP.insert(nextTri);
									left--;
								}
								if (left == 0 && trueCP == true)
								{
									remove = ComputeCriticalSetUp(z+offset, Critical_Upper, total1, TriContainingRoot, total);
									if (remove == 1)
									{
										getout = true;
										break;
									}
									else
									{
										trueCP = false;
									}
								}
							}
						}
						if (getout == true)
						{
							break;
						}                                       
					}        
						 if(Tag[nextTri])
                                                {
                                                        continue;
                                                }
                                                else
                                                {
//							std::stringstream sstr;
//						        sstr << nextTri;
//						        std::string str1 = sstr.str();
//							long int key = strtol(str1.c_str(), NULL, 10);
							CriticalSetComplete[z][nextTri] = component;
							innermap.push_back(nextTri);	
                                                        Tag[nextTri] = true;                                                        
                                                }
					
                                        }
                                }
				if (getout == true)
				{
					break;
				} 
			}
			if (getout == true)
			{
				break;
			}

		}
		if (getout == true)
		{		
			CPdifference++;
			getout = false;
			CriticalSetComplete[z].clear();
			remove = 0;			
		}

		z++;	
		getout = false;	
	}
	
}


int CTrist::ComputeCriticalSetUp(int CPIndex, std::map<int, int>* Critical_Upper, size_t total2, std::map<int, int>* TriContainingRoot, size_t total)
{
        int z = CPIndex;
	int criticalPoint = critical_Points[z];
        int triIndex;
        bool getout = false;
	int component = 0;
//	map1 innermap;
	std::list<int> innermap;
	std::vector<bool> Tag(maxTriangle, false);
	if (Critical_Upper[criticalPoint].empty())
	{
		return 0;
	}
	if (Critical_Upper[criticalPoint].size() == 1)
	{
//		cout << "Rempoved : " << critical_Points[z] << endl;
		critical_Points[z] = -1;
		return 1;
	}
	std::map<int, int>::iterator itt;
	itt = Critical_Upper[criticalPoint].begin();
	component++;
	set<int> queueCP;
	int left = (int)Critical_Upper[criticalPoint].size() - 1;
	triIndex = TriContainingRoot[criticalPoint][Critical_Upper[criticalPoint][(*itt).first]];
	innermap.push_back(triIndex);

	Tag[triIndex] = true;
	std::list<int>::iterator iter;
	for (iter = innermap.begin(); iter != innermap.end(); iter++)
	{
		triIndex = *iter;
		unsigned int i, j, k, v;
		bool flag1, flag2, flag3;
		v = 0;
		int subs = dimType/3;
		i = (m_tr[triIndex].origin[vo[v]] & org_Mask); v = ve[v];
		j = (m_tr[triIndex].origin[vo[v]] & org_Mask); v = ve[v];
		k = (m_tr[triIndex].origin[vo[v]] & org_Mask);
		flag1 = comparisonUp(i, criticalPoint); flag2 = comparisonUp(j, criticalPoint); flag3 = comparisonUp(k, criticalPoint);
		int next[dimType - subs];
		if (flag1 == flag2)
		{
			for (int a = 0; a < dimType - subs; a++) 
			{
				next[a] = a + subs;
			}
		}
		if (flag1 == flag3)
		{
			for (int a = 0; a < dimType - subs; a++)
			{
				next[a] = a;
			}
		}
		if (flag2 == flag3)
		{
			 int a = 0; int a1 = 0;
                         while (a < subs)
                         {
                               next[a1] = a;
                               a++; a1++;
                         }
                         a = a + subs;
                         while (a < dimType)
                         {
                               next[a1] = a;
                               a++; a1++;
                         }
		}
		for (int a = 0; a < dimType - subs; a++)
		{
			int nextTri = (m_tr[triIndex].fnext[next[a]]);
			if (comp(Tri_Value_Min[nextTri], criticalPoint) && comp(criticalPoint, Tri_Value[nextTri]))
			{
				if (component == 1)
				{
					std::map<int, int>::iterator inneritt;
					inneritt = Critical_Upper[criticalPoint].begin();
					inneritt++;
					int count = component;
					for (; inneritt != Critical_Upper[criticalPoint].end(); inneritt++)
					{
						count++;
						if ( TriContainingRoot[criticalPoint][Critical_Upper[criticalPoint][(*inneritt).first]] == nextTri)
						{
							if (queueCP.find(nextTri) == queueCP.end())
							{
								queueCP.insert(nextTri);
								left--;
							}
							if (left == 0)
							{
								getout = true;
								break;								
							}
						}
					}
					if (getout == true)
					{
						break;
					}
				}
					if(Tag[nextTri]) 
					{
						continue;
					}
					else
					{
						innermap.push_back(nextTri);
						Tag[nextTri] = true;
					}
			}	
				
		}	
			if (getout == true)
			{
				break;
			}
		}
		if (getout == true)
		{
//			cout << "Rempoved : " << critical_Points[z] << endl;
			critical_Points[z] = -1;
			return 1;			
		}
		else
		{
			return 0;
		}
	
	return 0;
}        
