/*
 *	Copyright (C) 2010 Visualization & Graphics Lab (VGL), Indian Institute of Science
 *
 *	This file is part of libRG, a library to compute Reeb graphs.
 *
 *	libRG is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Lesser General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	libRG is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Lesser General Public License for more details.
 *
 *	You should have received a copy of the GNU Lesser General Public License
 *	along with libRG.  If not, see <http://www.gnu.org/licenses/>.
 *
 *	Author(s):	Aneesh Sood
 *	Version	 :	1.0
 *
 *	Modified by : Aneesh Sood.
 *	Date : 14th May 2011.
 *	Changes  : Added code to clear the contents of all containers at the beginning of 
 *  		   the execution. 
 */
 
#include <stdio.h>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <assert.h>
#include <string.h>

#include "CFileReader.hpp"

using namespace std;
	
void CFileReader::ReadData3D(const char* fname, std::vector<Vertex> & vlist, std::vector<Tet> & tlist) 
{
	FILE* fp = fopen(fname, "r");
        assert(fp);
        int ver = 0 , tetr = 0;
        if (!(fscanf(fp, "%d", &ver)))
	{
		return;
	}
	if (!(fscanf(fp, "%d", &tetr)))
	{
		return;
	}
       	int n = 0, i = 0;
	my_float v[4] = {0,0,0,0};
	n = ver;
	bool flag = false;
	int tem = 0;
	string input;
	vlist.clear();
	while (!flag)
	{
		cout << "Please enter the input function (should be x, y, z or f): ";
		getline(cin, input);
		
		if (input.length() != 1 || (input != "x" && input != "y" && input != "z" && input != "f"))
		{
			cout << "invalid input function!" << endl;
			continue;
		}
		flag = true;
	}
	if (input == "x")
	tem = 0;	
	if (input == "y")
        tem = 1;
	if (input == "z")
        tem = 2;
	if (input == "f")
	tem = 3;

	for(i = 0; i < n; i++)
	{
		if (fscanf(fp, "%lf %lf %lf %lf", v, v+1, v+2, v+3))
		{
			if (tem == 0)
			{
				 v[3] = v[0];
			}
			else
			{
				if (tem == 1)
				{
					v[3] = v[1];
				}
				else
				{
					if (tem == 2)
					{
						v[3] = v[2];
					}	
				}
			}
			Vertex vert(v, v[3]);
			vlist.push_back(vert);
		}
	}	

	n = 0; i = 0;
	unsigned int v1[4] = {0,0,0,0};
	n = tetr;
	tlist.clear();
	for(i = 0; i < n; i++)
	{
		if (fscanf(fp, "%d %d %d %d", v1, v1+1, v1+2,v1+3))
		{
			v1[0] += 1;
                        v1[1] += 1;
                        v1[2] += 1;
			v1[3] += 1;
			tlist.push_back(Tet(v1));
		}
	}	
	fclose(fp);
}

void CFileReader::ReadData2D(const char* fname, std::vector<Vertex> & vlist, std::vector<int> & m_trilist)
{
	fpos_t position;
	FILE* fp = fopen(fname, "r");
        assert(fp);
        int ver = 0 , tetr = 0, temp = 0;
        char format[4], line[100];
        char ftest[] = "OFF";
	string input;
	int tem = 0;
	vlist.clear();
	m_trilist.clear();
        if (!(fgets (format, 4, fp)))
	{
		return;
	}
        if (strcmp(format, ftest))
        {
                cout << "invalid file format. File should be in OFF format for 2D models." << endl;
                exit (0);
        } 
        if (!(fscanf(fp, "%d", &ver)))
	{
		return;
	}
        if (!(fscanf(fp, "%d", &tetr)))
	{
		return;
	}
	if (!(fscanf(fp, "%d", &temp)))
        {
                return;
        }
	
	fgetpos(fp, &position);
	if (!(fgets(line, 100, fp)))
	{
		return;
	}
	int ptr = 0, spc_cnt = 0;
	while(line[ptr] != '\n')
	{
		if (line[ptr] == ' ')
		{
			
			while(line[ptr] == ' ')
			{
				ptr++;
			}
			if (line[ptr] == '\n')
			break;
			spc_cnt++;
		}
		ptr++;
	}
	
	fsetpos(fp, &position);

	bool flag = false;
	bool flag_f = false;
	while (!flag)
	{
		if (flag_f == false)
		{
			cout << "Please enter the input function (should be x, y, z or f): ";
		}
		else
		{
			cout << "Please enter the input function (should be x, y or z): ";
		}
		getline(cin, input);
		
		if (input.length() != 1 || (input != "x" && input != "y" && input != "z" && input != "f"))
		{
			cout << "invalid input function!" << endl;
			continue;
		}
		if (input == "f" && spc_cnt != 3)
		{
			
			cout << "No F coordinate exists! Please enter the input function (should be x, y or z ): " << endl;
			flag_f = true;
			continue;
		}
		flag = true;
	}
	if (input == "x")
	tem = 0;	
	if (input == "y")
        tem = 1;
	if (input == "z")
        tem = 2;
	if (input == "f")
	tem = 3;
	my_float v[4] = {0,0,0,0};
	
        for(int i = 0; i < ver; i++)
        {
                if (fscanf(fp, "%lf %lf %lf", v, v+1, v+2))
                {
			if (tem != 3)
			{
				v[3] = v[tem];
			}
			else
			{
				if (!(fscanf (fp, "%lf", v+3)))
				{
					return;
				}		
				
			}
                        Vertex vert(v, v[3]);
                        vlist.push_back(vert);
                }
        }

        int n = tetr, i;
        unsigned int v1[4] = {0,0,0,0};
        for(i = 0; i < n; i++)
        {
                if (fscanf(fp, "%d %d %d %d", v1, v1+1, v1+2, v1+3))
                {
			m_trilist.push_back(v1[1] + 1);
                        m_trilist.push_back(v1[2] + 1);
                        m_trilist.push_back(v1[3] + 1);
                }
        }
	
        fclose(fp);
}
