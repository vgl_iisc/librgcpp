/*
 *	Copyright (C) 2010 Visualization & Graphics Lab (VGL), Indian Institute of Science
 *
 *	This file is part of libRG, a library to compute Reeb graphs.
 *
 *	libRG is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Lesser General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	libRG is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Lesser General Public License for more details.
 *
 *	You should have received a copy of the GNU Lesser General Public License
 *	along with libRG.  If not, see <http://www.gnu.org/licenses/>.
 *
 *	Author(s):	Aneesh Sood
 *	Version	 :	1.0
 *
 *	Modified by : -- 
 *	Date : --
 *	Changes  : --
 */
 
#include "CFmtConverter.hpp"
#include "CFileReader.hpp"
#include "CTrist.hpp"
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include "../include/reebgraph.hpp"

using namespace std;

void reebgraph::computeReebGraph(char** argv)
{
	struct timeval tv;
	time_t sttime;
	time_t endtime;
	time_t stmsec;
	time_t endmsec;
	std::istringstream buffer(argv[3]);
        int batch;
        buffer >> batch;
	CFileReader rdr;
	CFmtConverter cvtr(&rdr);
	//specify node file (list of vertices)
	// and ele file (list of tetrahedra)
	std::istringstream buffer1(argv[1]);
        int type;
        buffer1 >> type;
	cvtr.Read(argv[2], type);
	gettimeofday(&tv, NULL);
        sttime = tv.tv_sec;
        stmsec = tv.tv_usec;
        CTrist* st = cvtr.CreateTrist(batch);
	if (type == 2){
	st->Declare();
	}
	char filename[] = "output.rg";
	if (argv[4] != NULL)
	{
		ofstream SaveFile(argv[4]);
		SaveFile.close();
	}
	else
	{
		argv[4] = filename;
		ofstream SaveFile("output.rg");
                SaveFile.close();
	}
	st->List(argv[4]);
	gettimeofday(&tv, NULL);
        endtime = tv.tv_sec;
	endmsec = tv.tv_usec;	
	double em = (double)endmsec;
        em = em/1000000;
        double sm = (double)stmsec;
        sm = sm/1000000;
        double start = (double)sttime;
        start = start + sm;
        double stop  = (double)endtime;
        stop = stop + em;
        cout << "Time taken to compute Reeb graph: " << stop - start << " " << "secs" << endl;
	
	
}
