/*
 *	Copyright (C) 2010 Visualization & Graphics Lab (VGL), Indian Institute of Science
 *
 *	This file is part of libRG, a library to compute Reeb graphs.
 *
 *	libRG is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Lesser General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	libRG is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Lesser General Public License for more details.
 *
 *	You should have received a copy of the GNU Lesser General Public License
 *	along with libRG.  If not, see <http://www.gnu.org/licenses/>.
 *
 *	Author(s):	Aneesh Sood
 *	Version	 :	1.0
 *
 *	Modified by : -- 
 *	Date : --
 *	Changes  : --
 */
 
#ifndef CFMTCONVERTER_HPP
#define CFMTCONVERTER_HPP

#include <vector>
#include <map>
#include <assert.h>

#include "Elements.hpp"

struct TriInfo
{
	unsigned int fnext[6];
	unsigned int id;
	TriInfo() : id(0)
	{ memset(fnext, 0, sizeof(fnext)); } 
	void Set(unsigned int next[6])
	{
		for(unsigned int i = 0; i <6; i++)
			if(next[i])
			{
				//assert(!fnext[i]);
				fnext[i] = next[i];
			}				
	}
	bool Filled(unsigned int pos[3])
	{
		bool bret = false;
		if(!fnext[1])
		{
			pos[0] = 1; assert(!fnext[1] && fnext[0]);
			pos[1] = 3; assert(!fnext[3] && fnext[2]);
			pos[2] = 5; assert(!fnext[5] && fnext[4]);
		}
		else if(!fnext[0])
		{
			pos[0] = 0; assert(fnext[1] && !fnext[0]);
			pos[1] = 2; assert(fnext[3] && !fnext[2]);
			pos[2] = 4; assert(fnext[5] && !fnext[4]);
		}
		else
		{ 
			assert(fnext[0] && fnext[1] && fnext[2] && 
				fnext[3] && fnext[4] && fnext[5]);
			bret = true;
		}
		return bret;
	}
};

struct EdgeInfo
{
	unsigned int trid;
	unsigned int v;
	std::map<Tri, TriInfo>::iterator it;
	EdgeInfo() : trid(0), v(0), it(0) {}
	EdgeInfo(unsigned id, unsigned pos, 
		const std::map<Tri, TriInfo>::iterator & itr) :
		trid(id), v(pos), it(itr) {}
};

class CFmtConverter
{
public:
	CFmtConverter(class CFileReader *rdr); 
	void Read(const char* fname, int type);
	//std::vector<Vertex> m_vertlist;
	class CTrist* CreateTrist(int batch);
	void CopyVertexList(std::vector<Vertex> & vertexlist);
	void CopyVertexTriList(std::vector<Vertex> & vertexlist, std::vector<int> & trilist);
	void List();
	void SortTetTri();
	void ListTriFnexts();
private:
	void ProcessTets();
	unsigned int Index(unsigned int val, const unsigned int verts[3]);
	unsigned int Pos(unsigned int val1, unsigned int val2, 
				const unsigned int verts[3]);
	void OrientTet(Tet & tet);
	void SplitToTri(const Tet & tet, unsigned int tri[4][3]);
	void SetInnerFNexts(const Tet & tet, unsigned int tri[4][3]);
	void SetOuterFNexts(const std::map<Tri, TriInfo>::iterator & it);
	void Order(unsigned int & a, unsigned int & b); 
	
	CFileReader *m_rdr;	
	//std::vector<Vertex> m_vertlist;
	std::vector<Tet> m_tetlist;
	std::map<Tri, TriInfo> trimap;
	std::map<Edge, EdgeInfo> edgemap;

	static unsigned int edgepos[3][3];
	static unsigned int posedge[6][2];
	static unsigned int fnextpos[4][3];
	static unsigned int nodepos[4][3];
	static unsigned int trcnt;
};

#endif
