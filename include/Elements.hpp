/*
 *	Copyright (C) 2010 Visualization & Graphics Lab (VGL), Indian Institute of Science
 *
 *	This file is part of libRG, a library to compute Reeb graphs.
 *
 *	libRG is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Lesser General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	libRG is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Lesser General Public License for more details.
 *
 *	You should have received a copy of the GNU Lesser General Public License
 *	along with libRG.  If not, see <http://www.gnu.org/licenses/>.
 *
 *	Author(s):	Aneesh Sood
 *	Version	 :	1.0
 *
 *	Modified by : -- 
 *	Date : --
 *	Changes  : --
 */
 
#ifndef ELEMENTS_HPP
#define ELEMENTS_HPP

#include <string.h>
#define my_float double


struct Vertex
{
	my_float xyz[4];
	my_float density;
	my_float w;
	//Create a co-face?
	Vertex(my_float v[4], const my_float & d = 0, 
		const my_float & w1 = 0) : density(d), w(w1)
	{
		memcpy(xyz, v, sizeof(xyz));
	}
};

struct Edge
{
	unsigned int node[2];
	Edge(unsigned int n[2]) 
	{ memcpy(node, n, sizeof(node));}
	bool operator < (const Edge & rhs) const
	{
		return node[0] < rhs.node[0] || 
			((node[0] == rhs.node[0]) && (node[1] < rhs.node[1]));
	}
};

struct Tri
{
	unsigned int node[3];
	Tri(unsigned int n[3]) 
	{ memcpy(node, n, sizeof(node));}
	bool operator < (const Tri & rhs) const
	{
		return node[0] < rhs.node[0] || 
			((node[0] == rhs.node[0]) && (node[1] < rhs.node[1])) 
			|| (node[0] == rhs.node[0] && node[1] == rhs.node[1]
					&& node[2] < rhs.node[2]);
	}
};

struct Tet
{
	unsigned int node[4];
	Tet(unsigned int n[4]) 
	{ memcpy(node, n, sizeof(node));}
	bool operator < (const Tet & r) const
	{
		return (node[0] < r.node[0]) || (node[0] == r.node[0] 
				&& node[1] < r.node[1]) ||
			(node[0] == r.node[0] && node[1] == r.node[1]
			&& node[2] < r.node[2]) ||
		(node[0] == r.node[0] && node[1] == r.node[1] &&
		node[2] == r.node[2] && node[3] < r.node[3]);
	}
};

#endif
