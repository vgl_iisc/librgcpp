/*
 *	Copyright (C) 2010 Visualization & Graphics Lab (VGL), Indian Institute of Science
 *
 *	This file is part of libRG, a library to compute Reeb graphs.
 *
 *	libRG is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Lesser General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	libRG is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Lesser General Public License for more details.
 *
 *	You should have received a copy of the GNU Lesser General Public License
 *	along with libRG.  If not, see <http://www.gnu.org/licenses/>.
 *
 *	Author(s):	Aneesh Sood
 *	Version	 :	1.0
 *
 *	Modified by : -- 
 *	Date : --
 *	Changes  : --
 */
 
#ifndef CTRIST_HPP
#define CTRIST_HPP

#include "Elements.hpp"
#include <map>
#include <vector>
#include <list>
#include <unordered_map>


#define org_Mask  ((Trist_org)  org_max) 



class CTrist
{

public:
	CTrist() : m_trist(0), m_tr(0) {};

	typedef std::multimap<int, int> map1;

	void Allocate(unsigned int max_n, unsigned int m, int bat);
	unsigned int MakeTri(const unsigned int verts[3], 
		unsigned int id = 0, unsigned int *fnext = 0, int type = 0);

	
	unsigned int Sym(unsigned int e);
	unsigned int Dest (unsigned int e);
	unsigned int Apex(unsigned int e);
	unsigned int Turn(unsigned int e);
	unsigned int Enext (unsigned int e);
	unsigned int Enext2 (unsigned int e);
	unsigned int Fnext (unsigned int e);
	unsigned int Org (unsigned int e);

	void CheckConsistency();
	void Declare();
	void Fill(int v, int u, int i, int w, std::map<int, int>* trace, size_t space, std::map<int, int>* EdgeID, size_t total);


	void stepIn(std::map<int, int>* TriContainingRoot, size_t total, std::map<int, int>* Critical_Upper, size_t total1, std::map<int, int>* Critical_Lower, size_t total2, std::map<int, int>* Up_Star, size_t total3);
	void Print(int e, std::map<int, int>* TriContainingRoot, size_t total5, std::map<int, int>* upper, size_t total, std::map<int, int>* Critical_Upper, size_t total1, std::map<int, int>* Critical_Lower, size_t total2, std::map<int, int>* lower, size_t total4, std::map<int, int>* Up_Star, size_t total3);
	void Print2D(std::map<int, int>* TriContainingRoot, size_t total5, std::map<int, int>* upper, size_t total, std::map<int, int>* Critical_Upper, size_t total1, std::map<int, int>* Critical_Lower, size_t total2, std::map<int, int>* lower, size_t total4, std::map<int, int>* Up_Star, size_t total3);
	void Traverse(int i1, int j1, int k1, int t, std::map<int, int>* Critical_Upper, size_t total5, std::map<int, int>* Critical_Lower, size_t total2, std::map<int, int>* TriContainingRoot, size_t total, std::map<int, int>* upper, size_t total1, std::map<int, int>* lower, size_t total6);
	int Find(int flag,int cv, int x, std::map<int, int>* upper, size_t total1, std::map<int, int>* lower, size_t total2);
	void List(char* filename);


	void ComputeReebgraph(std::map<int, int>* Critical_Lower, size_t total2, std::map<int, int>* TriContainingRoot, size_t total, std::map<int, int>* Critical_Upper, size_t total1, std::map<int, int>* Up_Star, size_t total3, int cur, int batch);
	void ConnectCriticalPoints(std::map<int, int>* TriContainingRoot, size_t total, std::map<int, int>* Critical_Upper, size_t total1, std::map<int, int>* Critical_Lower, size_t total2, std::map<int, int>* Up_Star, size_t total3, std::unordered_map<int, int>* CriticalSetComplete, size_t total5, int cur, int batch);
	void CalculateCriticalSet(std::map<int, int>* TriContainingRoot, size_t total, std::map<int, int>* Critical_Upper, size_t total1, 
	                                  std::map<int, int>* Critical_Lower, size_t total2, std::multimap<int, int>* CriticalSet, size_t total3);
	int TravelUp(std::map<int, int>* TriContainingRoot, size_t total, std::map<int, int>* Critical_Upper, size_t total1,std::map<int, int>* Critical_Lower, size_t total2, int Index, int criticalPoint, int level, std::unordered_map<int, int>* CriticalSetComplete, size_t total3, std::map<int, int>* Up_Star, size_t total4, int component, bool flag, int extent, int cur);

	 bool ComputeSet(std::vector<std::vector<bool> > &Tri_Tag,std::map<int, int>::iterator* cit, size_t number, int secondLevel, std::map<int, int>* Critical_Lower, size_t total2, std::map<int, int>* TriContainingRoot, size_t total, int triangle, int criticalPoint, int componentNo, double value, std::multimap<int, int>* CriticalSet, size_t total3);
	void ComputeCriticalSet(std::map<int, int>* Critical_Lower, size_t total2, std::map<int, int>* TriContainingRoot, size_t total, std::unordered_map<int, int>* CriticalSetComplete, size_t total5, std::map<int, int>* Critical_Upper, size_t total1, int cur, int batch);
	int ComputeCriticalSetUp(int CPIndex, std::map<int, int>* Critical_Upper, size_t total1, std::map<int, int>* TriContainingRoot, size_t total);
	void CopyTriMap(std::multimap<int, int> TriCopy);
	CTrist(class CFmtConverter *rdr);
	void Output(char* filename);

	
private:
	typedef   unsigned int  Trist_org;
	struct  Trist_record
	{
		Trist_org origin[3];
		unsigned int fnext[6];
	};
	struct Trist
	{
		unsigned int max_org;
		unsigned int max_triangle;
		Trist_record *triangle;  /* [1..last_triangle]; entry [0] is unused! */
		unsigned int last_triangle;
		unsigned int used_triangles;
		//unsigned int *Vcoface;/* -- Vijay */
	};


	void Clear();
	unsigned int TrIndex(unsigned int t)
	{ return t >> 3; }
	unsigned int TrVersion(unsigned int t)
	{ return t & 07; }
	unsigned int Odd(unsigned int e)
	{ return (e % 2) == 1; }
	
	CFmtConverter *reader;
	static unsigned int org_max;
	static unsigned int vo[6]; /* origins wrt triangle version */
	static unsigned int ve[6]; /* enexts  ... */
	Trist* m_trist;
	Trist_record *m_tr;  /* shortcut for st->triangle */


};

inline unsigned int EdFacet(unsigned int t, unsigned int v)
{ return ((t<<3) + v); }


#endif
